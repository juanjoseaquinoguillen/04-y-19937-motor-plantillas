const express = require('express');
const router = express.Router();
const { getALL, db } = require('../db/conexion'); 
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: './public/assets' });
const fileUpload = upload.single('imagen');
const sqlite3 = require('sqlite3');
const path = require('path');

// Añadir el helper 'eq' para comparar valores directamente en la plantilla
const hbs = require('hbs');
hbs.registerHelper('eq', function (a, b) {
    return a == b;
});

router.get('/', (req, res) => {
    res.render('admin/index');
});
router.get('/Integrante/listar', async (req, res) => {
    try {
        const integrantes = await getALL(`SELECT * FROM Integrante`);

        if (integrantes.length === 0) {
            return res.render("admin/Integrante/index", { message: "No hay integrantes", integrantes: [] });
        }

        // Convertir el estado de esta_borrado a "Activo" o "Inactivo"
        const processedIntegrantes = await Promise.all(integrantes.map(async integrante => {
            const datos = await getALL(`SELECT * FROM media WHERE matricula = '${integrante.matricula}'`);
            return {
                ...integrante,
                esta_borrado: integrante.esta_borrado ? 'Inactivo' : 'Activo',
                datos
            };
        }));

        res.render("admin/Integrante/index", { integrantes: processedIntegrantes });
    } catch (error) {
        console.error(error);
        return res.status(500).render("error");
    }
});

router.get('/Media/listar', async (req, res) => {
    try {
        const mediaItems = await getALL(`SELECT * FROM Media`);

        if (mediaItems.length === 0) {
            return res.render("admin/Media/index", { message: "No hay elementos de media", mediaItems: [] });
        }

        // Convertir el estado de esta_borrado a "Activo" o "Inactivo"
        const processedMediaItems = mediaItems.map(item => ({
            ...item,
            esta_borrado: item.esta_borrado ? 'Inactivo' : 'Activo'
        }));

        res.render("admin/Media/index", { mediaItems: processedMediaItems });
    } catch (error) {
        console.error(error);
        return res.status(500).render("error");
    }
});

router.get('/TipoMedio/listar', async (req, res) => {
    try {
        const tipoMedioItems = await getALL(`SELECT * FROM TipoMedio`);

        if (tipoMedioItems.length === 0) {
            return res.render("admin/TipoMedio/index", { message: "No hay elementos de tipo de medio", tipoMedioItems: [] });
        }

        // Convertir el estado de esta_borrado a "Activo" o "Inactivo"
        const processedItems = tipoMedioItems.map(item => ({
            ...item,
            esta_borrado: item.esta_borrado ? 'Inactivo' : 'Activo'
        }));

        res.render("admin/TipoMedio/index", { tipoMedioItems: processedItems });
    } catch (error) {
        console.error(error);
        return res.status(500).render("error");
    }
});

router.get('/Integrante/crear', (req, res) => {
    const mensaje = req.query.mensaje;
    const datosForm = req.query;
    res.render('admin/Integrante/crearForm', {
        mensaje: mensaje,
        datosForm: datosForm,
    });
});

router.get('/TipoMedio/crear', (req, res) => {
    const mensaje = req.query.mensaje;
    const datosForm = req.query;
    res.render('admin/TipoMedio/crearForm', {
        mensaje: mensaje,
        datosForm: datosForm,
    });
});
router.get('/Media/crear', (req, res) => {
    const mensaje = req.query.mensaje;
    const datosForm = req.query;
    res.render('admin/Media/crearForm', {
        mensaje: mensaje,
        datosForm: datosForm,
    });
});

 
 
router.post('/Integrante/create', async (req, res) => {
    try {
        const { matricula, nombre, apellido, rol, codigo, url, esta_borrado } = req.body;

        const normalizedRol = rol ? rol.toLowerCase() : '';

        if (normalizedRol && normalizedRol !== 'desarrollador' && normalizedRol !== 'ayudante') {
            const queryParams = new URLSearchParams(req.body).toString();
            return res.redirect(`/admin/Integrante/crear?mensaje=El rol debe ser "Desarrollador" o "Ayudante"&${queryParams}`);
        }

        const maxOrdenResult = await new Promise((resolve, reject) => {
            db.get("SELECT MAX(orden) as maxOrden FROM Integrante", (err, row) => {
                if (err) {
                    return reject(err);
                }
                resolve(row);
            });
        });

        const newOrden = (maxOrdenResult.maxOrden || 0) + 1;
        db.run(
            "INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            [matricula, nombre, apellido, rol, codigo, url, esta_borrado, newOrden],
            (err) => {
                if (err) {
                    console.error("Error al insertar en la base de datos:", err);
                    return res.sendStatus(500);
                }
                res.redirect("/admin/Integrante/crear?mensaje=Integrante creado exitosamente");
            }
        );
    } catch (error) {
        console.error('Error al insertar en la base de datos:', error);
        res.status(500).send('Error al insertar en la base de datos');
    }
});

router.post('/TipoMedio/create', async (req, res) => {
    try {
        const { tipo, descripcion, esta_borrado } = req.body;

        if (!tipo || !descripcion) {
            const queryParams = new URLSearchParams(req.body).toString();
            return res.redirect(`/admin/TipoMedio/crear?mensaje=Todos los campos son obligatorios&${queryParams}`);
        }

        const tipoExistente = await new Promise((resolve, reject) => {
            db.get("SELECT COUNT(*) as count FROM TipoMedio WHERE tipo = ?", [tipo], (err, row) => {
                if (err) {
                    return reject(err);
                }
                resolve(row.count > 0);
            });
        });

        if (tipoExistente) {
            const queryParams = new URLSearchParams(req.body).toString();
            return res.redirect(`/admin/TipoMedio/crear?mensaje=El tipo ya existe&${queryParams}`);
        }

        const maxOrdenResult = await new Promise((resolve, reject) => {
            db.get("SELECT MAX(orden) as maxOrden FROM TipoMedio", (err, row) => {
                if (err) {
                    return reject(err);
                }
                resolve(row);
            });
        });
        const newOrden = (maxOrdenResult.maxOrden || 0) + 1;

        db.run(
            "INSERT INTO TipoMedio (tipo, descripcion, esta_borrado, orden) VALUES (?, ?, ?, ?)",
            [tipo, descripcion, esta_borrado, newOrden],
            (err) => {
                if (err) {
                    console.error("Error al insertar en la base de datos:", err);
                    return res.sendStatus(500);
                }
                res.redirect("/admin/TipoMedio/crear?mensaje=Tipo de Medio creado exitosamente");
            }
        );
    } catch (error) {
        console.error('Error al insertar en la base de datos:', error);
        res.status(500).send('Error al insertar en la base de datos');
    }
});

router.post('/Media/create', fileUpload, async (req, res) => {
    try {
        const { matricula, url, titulo, parrafo, video, tipo_medio, esta_borrado } = req.body;
        let imagenPath = '';
        let embedUrl = '';

        const matriculaRegex = /^Y\d{5}$/;
        if (!matriculaRegex.test(matricula)) {
            const queryParams = new URLSearchParams(req.body).toString();
            return res.redirect(`/admin/Media/crear?mensaje=La matrícula debe comenzar con 'Y' seguido de 5 dígitos&${queryParams}`);
        }

        if (req.file) {
            const nombreArchivo = titulo.replace(/\s+/g, '-').toLowerCase();
            const extension = path.extname(req.file.originalname);
            const nuevoNombreArchivo = `${nombreArchivo}-${Date.now()}${extension}`;
            
            fs.renameSync(req.file.path, path.join(req.file.destination, nuevoNombreArchivo));
            imagenPath = path.join(req.file.destination, nuevoNombreArchivo).replace(/\\/g, '/');
        }

        if (video) {
            const videoIdMatch = video.match(/(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/)|youtu\.be\/)([^\s&]+)/);
            if (videoIdMatch) {
                const videoId = videoIdMatch[1];
                embedUrl = `https://www.youtube.com/embed/${videoId}`;
            } else {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/Media/crear?mensaje=URL de YouTube no válida&${queryParams}`);
            }
        }

        if (!matricula || !url || !titulo || !parrafo || !tipo_medio) {
            const queryParams = new URLSearchParams(req.body).toString();
            return res.redirect(`/admin/Media/crear?mensaje=Todos los campos son obligatorios&${queryParams}`);
        }

        const maxOrdenResult = await new Promise((resolve, reject) => {
            db.get("SELECT MAX(orden) as maxOrden FROM Media", (err, row) => {
                if (err) {
                    return reject(err);
                }
                resolve(row);
            });
        });

        const newOrden = (maxOrdenResult.maxOrden || 0) + 1;

        db.run(
            "INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, tipo_medio, esta_borrado, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [matricula, url, titulo, parrafo, imagenPath, embedUrl, tipo_medio, esta_borrado, newOrden],
            (err) => {
                if (err) {
                    console.error("Error al insertar en la base de datos:", err);
                    return res.sendStatus(500);
                }
                res.redirect("/admin/Media/crear?mensaje=Media creado exitosamente");
            }
        );
    } catch (error) {
        console.error('Error al insertar en la base de datos:', error);
        res.status(500).send('Error al insertar en la base de datos');
    }
});


module.exports = router;

