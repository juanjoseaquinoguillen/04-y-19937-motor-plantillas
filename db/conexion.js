const sqlite3 = require("sqlite3").verbose();

const db = new sqlite3.Database(
  "./db/integrantes.sqlite",
  sqlite3.OPEN_READWRITE,
  (error) => {
    if (error) console.log("Ocurrio un error", error.message);
    else {
      console.log("Conexion Exitosa");
      db.run("select * from integrante");
    }
  }
);

async function getALL(query) {
  return new Promise((resolve, reject) => {
    db.all(query, (error, rows) => {
      if (error) return reject(err);
      resolve(rows);
    });
  });
}

module.exports = { db, getALL };
