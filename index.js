const express = require('express');
const hbs = require('hbs');
const app = express();
require('dotenv').config({ path: '.env' });
const bodyParser = require("body-parser");
const multer = require ("multer");
const upload = multer ({dest:"./public/assets"});
const fileUpload = upload.single("imagen");
//const db = require( './db/data' );anterior llamado a la bd simulada
//importar archivo de rutas

const router = require('./routes/public');
const routerAdmin = require('./routes/admin');

// Utilizar los routers
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use("/", router);
app.use("/admin", routerAdmin);

//creacion de aplicacion express
app.use(express.static('public'));
app.use("/admin", express.static('admin'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

hbs.registerPartials(__dirname + "/views/partials");
hbs.registerHelper('extractYouTubeID', function(url) {
    const regex = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/;
    const match = url.match(regex);
    return match ? match[1] : null;
});


const puerto = process.env.PORT || 3000;
app.listen(puerto, () => {
    console.log(`El servidor se está ejecutando en el puerto ${puerto}`);
});
